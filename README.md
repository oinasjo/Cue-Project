# Cue-Project
Queue media from other websites for other users to listen to in a public one-way online radio

## Documentation
*Add confluence?*

## Jira
This project has a Jira - board. Ask the owner for a link.

## Docker
This project has been dockerized. Simply run `docker-compose up --build` to start the whole stack after installing docker - tools to your workstation!

## Version control
`https://bitbucket.org/Supersofty/cue-project/src/master/`

`https://gitlab.com/oinasjo/Cue-Project`
